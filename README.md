# 120 secondes

120 secondes est un exerciseur portant sur les 4 opérations. Le but du jeu est de répondre correctement à un maximum de calculs en 2 minutes.À chaque fois que le score passe à la dizaine supérieure, le niveau augmente et les calculs sont un peu plus compliqués !

https://www.multimaths.net/120s.php